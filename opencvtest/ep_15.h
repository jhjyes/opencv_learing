#ifndef _ep_15_h_
#define _ep_15_h_
#include <opencv.hpp>
using namespace cv;
using namespace std;
void ep_15();
void colorReduce_pt(Mat& inputImage, Mat& outputImage, int div);
void colorReduce_it(Mat& inputImage, Mat& outputImage, int div);
void colorReduce_adr(Mat& inputImage, Mat& outputImage, int div);
#endif // !_ep_15_h
#pragma once
