#ifndef _ep_17_h_
#define _ep_17_h_
#include<opencv.hpp>
#include <io.h>
#include <string>

int ep_17();
int predict(char* src);
int train();
uint32_t  reverseDigit(uint32_t val);
void testMnist();
extern cv::Mat readImages(int opt);
extern cv::Mat readLabels(int opt);
void knnTrain();
void testMnist();
#endif // !_ep_17_h_

#pragma once
