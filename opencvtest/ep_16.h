#ifndef _ep_16_h_
#define _ep_16_h_
#include<opencv.hpp>
//-----------------------------------【全局函数声明部分】--------------------------------------
//	描述：全局函数声明
//-----------------------------------------------------------------------------------------------
bool  ROI_AddImage();
bool  LinearBlending();
bool  ROI_LinearBlending();
void ep_16();

#endif // !_ep_16_h_


#pragma once
