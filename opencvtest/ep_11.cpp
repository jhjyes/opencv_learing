#include"ep_11.h"
using namespace cv;
using namespace std;


const int g_Max_Value=100;//滑动条的最大值
int g_trackbar_varity=70;//滑动条对应的变量

cv::Mat img = imread("1.jpg");
cv::Mat logo = imread("2.jpg");
cv::Mat add_logo;

void ep_11() {
	
	namedWindow("原图+logo",1);
	//add_logo = img(Rect(800, 350, logo.rows, logo.cols));//在img上开辟一片区域
	
	
	char trackbar_name[50];
	sprintf_s(trackbar_name, "透明值：", g_Max_Value);
	createTrackbar(trackbar_name, "原图+logo", &g_trackbar_varity, g_Max_Value, on_trackbar);
	
	on_trackbar(g_trackbar_varity, 0);
	waitKey();
}
void on_trackbar(int,void*) {
	double g_recent_precentage = (double)g_trackbar_varity / g_Max_Value;
	double g_recent_value = 1 - g_recent_precentage;
	addWeighted(img, g_recent_precentage, logo, g_recent_value, 0, add_logo);//把logo加到所在区域
	imshow("原图+logo", add_logo);

}