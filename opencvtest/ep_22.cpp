#include "ep_22.h"
using namespace std;
using namespace cv;
static Mat srcimg = imread("girl.jpg");
static Mat dstimg = srcimg.clone();
static int medianBlurValue=1;
static int bilateralFilterValue = 1;
void ep_22()
{
	namedWindow("Դͼ��", WINDOW_AUTOSIZE);
	imshow("Դͼ��", srcimg);
	namedWindow("��ֵ�˲�", WINDOW_AUTOSIZE);
	createTrackbar("����ֵ��", "��ֵ�˲�", &medianBlurValue, 100, on_medianBlur);
	on_medianBlur(medianBlurValue,0);

	namedWindow("˫���˲�", WINDOW_AUTOSIZE);
	createTrackbar("����ֵ��", "˫���˲�", &bilateralFilterValue, 100, on_bilateralFilter);
	on_bilateralFilter(bilateralFilterValue, 0);
	waitKey(0);
}

void on_medianBlur(int medianBlurValue, void*)
{
	medianBlur(srcimg, dstimg, medianBlurValue*2+1);
	imshow("��ֵ�˲�", dstimg);
}

void on_bilateralFilter(int bilateralFilterValue, void*)
{
	bilateralFilter(srcimg, dstimg, bilateralFilterValue, bilateralFilterValue * 2, bilateralFilterValue / 2);
	imshow("˫���˲�", dstimg);
}
