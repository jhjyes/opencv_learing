#include "ep_18.h"
#include <opencv.hpp>
#include <fstream>
using namespace std;
using namespace cv;

void ep_18() {
    //knnTrain();
    testMnist();
}
uint32_t  reverseDigit(uint32_t val) {
    val = ((val << 8) & 0xFF00FF00) | ((val >> 8) & 0xFF00FF);
    return (val << 16) | (val >> 16);

}
Mat readImages(int opt) {
    int idx = 0;
    ifstream file;
    Mat img;
    if (opt == 0)
    {
        cout << "\n Training...";
        file.open("D:\\cplusplus_program\\opencvtest\\opencvtest\\train-images.idx3-ubyte", ios::binary);
    }
    else
    {
        cout << "\n Test...";
        file.open("D:\\cplusplus_program\\opencvtest\\opencvtest\\t10k-images.idx3-ubyte", ios::binary);
    }
    // check file
    if (!file.is_open())
    {
        cout << "\n File Not Found!";
        return img;
    }
    /*
    byte 0 - 3 : Magic Number(Not to be used)
    byte 4 - 7 : Total number of images in the dataset
    byte 8 - 11 : rows of each image in the dataset
    byte 12 - 15 : cols of each image in the dataset
    */
    int magic_number = 0;//文件中的魔术数(magic number) 
    int number_of_images = 0;//mnist图像集文件中的图像数目
    int height = 0;//图像的行数
    int width = 0;//图像的列数
    int value = sizeof(magic_number);
    file.read((char*)&magic_number, sizeof(magic_number));
    magic_number = reverseDigit(magic_number);

    file.read((char*)&number_of_images, sizeof(number_of_images));
    number_of_images = reverseDigit(number_of_images);

    file.read((char*)&height, sizeof(height));
    height = reverseDigit(height);

    file.read((char*)&width, sizeof(width));
    width = reverseDigit(width);

    Mat train_images = Mat(number_of_images, height * width, CV_8UC1, Scalar::all(0));
    cout << "\n No. of images:" << number_of_images << endl;
    Mat digitImg = Mat::zeros(height, width, CV_8UC1);
    for (int i = 0; i < number_of_images; i++) {//第i副图像
        int index = 0;
        for (int r = 0; r < height; ++r) {//第r行
            for (int c = 0; c < width; ++c) {//第c列
                unsigned char temp = 0;
                file.read((char*)&temp, sizeof(temp));
                index = r * width + c;
                train_images.at<uchar>(i, index) = (int)temp;
                digitImg.at<uchar>(r, c) = (int)temp;
            }
        }
        if (i < 100) {
            imwrite(format("D:\\cplusplus_program\\opencvtest\\opencvtest\\digit_%d.png", i), digitImg);
        }
    }
    train_images.convertTo(train_images, CV_32FC1);
    return train_images;
}

Mat readLabels(int opt) {
    int idx = 0;
    ifstream file;
    Mat img;
    if (opt == 0)
    {
        cout << "\n Training...";
        file.open("D:\\cplusplus_program\\opencvtest\\opencvtest\\train-labels.idx1-ubyte");
    }
    else
    {
        cout << "\n Test...";
        file.open("D:\\cplusplus_program\\opencvtest\\opencvtest\\t10k-labels.idx1-ubyte");
    }
    // check file
    if (!file.is_open())
    {
        cout << "\n File Not Found!";
        return img;
    }
    /*
    byte 0 - 3 : Magic Number(Not to be used)
    byte 4 - 7 : Total number of labels in the dataset
    */
    int magic_number = 0;
    int number_of_labels = 0;

    file.read((char*)&magic_number, sizeof(magic_number));
    magic_number = reverseDigit(magic_number);
    file.read((char*)&number_of_labels, sizeof(number_of_labels));
    number_of_labels = reverseDigit(number_of_labels);

    cout << "\n No. of labels:" << number_of_labels << endl;
    Mat labels = Mat(number_of_labels, 1, CV_8UC1);
    for (long int i = 0; i < number_of_labels; ++i)
    {
        unsigned char temp = 0;
        file.read((char*)&temp, sizeof(temp));
        //printf("temp : %d\n ", temp);
        labels.at<uchar>(i, 0) = temp;
    }
    labels.convertTo(labels, CV_32SC1);
    return labels;
}

void knnTrain() {
    Mat train_images = readImages(0);
    Mat train_labels = readLabels(0);
    printf("\n read mnist train dataset successfully...\n");
    Ptr<ml::KNearest> knn = ml::KNearest::create();
    knn->setDefaultK(5);
    knn->setIsClassifier(true);
    Ptr<ml::TrainData> tdata = ml::TrainData::create(train_images, ml::ROW_SAMPLE, train_labels);
    knn->train(tdata);
    knn->save("D:\\cplusplus_program\\opencvtest\\opencvtest\\knn_knowledge.yml");
}

void testMnist() {
    //Ptr<ml::SVM> svm = Algorithm::load<ml::SVM>("D:/vcprojects/images/mnist/knn_knowledge.yml"); // SVM-POLY - 98%
    //读取历史训练数据
    Ptr<ml::KNearest> knn = Algorithm::load<ml::KNearest>("D:\\cplusplus_program\\opencvtest\\opencvtest\\knn_knowledge.yml"); // KNN - 97%
    Mat train_images = readImages(1);
    Mat train_labels = readLabels(1);
    printf("\n read mnist test dataset successfully...\n");

    float total = train_images.rows;
    float correct = 0;
    Rect rect;
    rect.x = 0;
    rect.height = 1;
    rect.width = (28 * 28);
    for (int i = 0; i < total; i++) {
        int actual = train_labels.at<int>(i);
        rect.y = i;
        Mat oneImage = train_images(rect);
        //int digit = svm->predict(oneImage);
        Mat result;
        float predicted = knn->predict(oneImage, result);
        int digit = static_cast<int>(predicted);
        if (digit == actual) {
            correct++;
        }
    }
    printf("\n recognize rate : %.2f \n", correct / total);
}
