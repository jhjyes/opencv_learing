#ifndef _ep_22_h_
#define _ep_22_h_
#include<opencv.hpp>
#include<iostream>

void ep_22();
void on_medianBlur(int medianBlurValue, void *);
void on_bilateralFilter(int bilateralFilterValue, void *);
#endif // !_ep_22_h_
#pragma once
