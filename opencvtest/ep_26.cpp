#include "ep_26.h"
using namespace std;
using namespace cv;

void ep_26()
{
	Mat srcimg, dstimg1, dstimg2;
	srcimg = imread("doge.jpg");
	imshow("原始图", srcimg);
	resize(srcimg, dstimg1, Size(srcimg.cols / 2, srcimg.rows / 2), (0, 0), (0, 0), 3);
	resize(srcimg, dstimg2, Size(srcimg.cols * 2, srcimg.rows * 2), (0, 0), (0, 0), 3);
	imshow("目标图1", dstimg1);
	imshow("目标图2",dstimg2);
	waitKey(0);
}
