#include "ep_23.h"
using namespace std;
using namespace cv;

static Mat srcimg = imread("doge.jpg");
static Mat dstimg = srcimg.clone();
static int SlectType = 0;
static int kernlSize = 3;

void ep_23()
{
	namedWindow("源图像", WINDOW_AUTOSIZE);
	imshow("源图像", srcimg);

	namedWindow("效果图", WINDOW_AUTOSIZE);
	createTrackbar("腐蚀/膨胀", "效果图", &SlectType, 1, on_ValueChange);
	on_ValueChange(SlectType,0);
	

	createTrackbar("内核尺寸", "效果图", &kernlSize, 21, on_ValueChange);
	on_ValueChange(kernlSize, 0);
	waitKey(0);
}

//void on_TypeChange(int SlectType, void*) {
//	if (SlectType == 0) {
//
//	}
//}
//
//void on_SizeChange(int kernlSize,void *){
//
//}
void on_ValueChange(int,void*)
{
	
	Mat element = getStructuringElement(MORPH_RECT, Size(2 * kernlSize + 1, 2 * kernlSize + 1), Point(kernlSize, kernlSize));
	if (SlectType == 0) {
		erode(srcimg, dstimg, element);
		cout << "操作类型：" << SlectType << "\t" << "内核值：" << kernlSize << endl;
	}
	else if (SlectType == 1) {
		dilate(srcimg, dstimg, element);
		cout << "操作类型：" << SlectType << "\t" << "内核值：" << kernlSize << endl;
	}
	imshow("效果图", dstimg);

}