#include "ep_17.h"
//#include <opencv2/core/arithm.simd.hpp>
#include <fstream>
#include<math.h>



using namespace std;
using namespace cv;

int ep_17()
{
   /*char src[40];
   
    while (1) {
        cout << "input filename：";
        cin >> src;
        
        cout << src << endl;
       
        predict(src);
    }*/
    //train();
   
    knnTrain();
    testMnist();

    return 0;
}






int predict(char *src) {
    //将所有图片大小统一转化为8*16
    const int imageRows = 8;
    const int imageCols = 16;
    //读取训练结果
    Ptr<ml::ANN_MLP> model = ml::StatModel::load<ml::ANN_MLP>("MLPModel.xml");
   // ==========================预测部分==============================
        //读取测试图像
        Mat test, dst;
    test = imread(src, 0);;
    if (test.empty())
    {
        std::cout << "can not load image \n" << std::endl;
        return -1;
    }
    //将测试图像转化为1*128的向量
    resize(test, test, Size(imageRows, imageCols), (0, 0), (0, 0), INTER_AREA);
    threshold(test, test, 0, 255, THRESH_BINARY | THRESH_OTSU);
    Mat_<float> testMat(1, imageRows * imageCols);
    for (int i = 0; i < imageRows * imageCols; i++)
    {
        testMat.at<float>(0, i) = (float)test.at<uchar>(i / 8, i % 8);
    }
    //使用训练好的MLP model预测测试图像
    model->predict(testMat, dst);
    std::cout << "testMat: \n" << testMat << "\n" << std::endl;
    std::cout << "dst: \n" << dst << "\n" << std::endl;
    double maxVal = 0;
    Point maxLoc;
    minMaxLoc(dst, NULL, &maxVal, NULL, &maxLoc);
    std::cout << "测试结果：" << maxLoc.x << "置信度:" << maxVal * 100 << "%" << std::endl;
    namedWindow("原图", WINDOW_AUTOSIZE);
    imshow("原图", test);
    
}
int train() {

    // ==========================读取图片创建训练数据==============================
         //将所有图片大小统一转化为8*16
    const int imageRows = 8;
    const int imageCols = 16;
    //图片共有10类
    //string command_esedbexport = "dir /b | find /v /c "" ";
   // command_esedbexport += edgeBasePath;//这里的edgeBasePath就是外部的可变参数
    //const int classSum =system(command_esedbexport.c_str());
   // ShellExecute(NULL, "runas", "cmd", "/c ipconfig >> D:\\disk.txt", NULL, SW_SHOWNORMAL);

    //const int classSum = system("dir /b | find /v /c "">1.log");
    const int classSum = 34;
    //每类共50张图片
    const int imagesSum = 50;
    //每一行一个训练图片
    Mat trainingData(classSum * imagesSum, imageRows * imageCols, CV_32FC1, Scalar::all(0));
    //float trainingData[classSum * imagesSum][imageRows * imageCols] = { {0} };
    //训练样本标签
    Mat labels(classSum * imagesSum, classSum, CV_8UC1, Scalar::all(0));
    // float labels[classSum * imagesSum][classSum] = { {0} };
    Mat  resizeImg(imageRows, imageCols, CV_32FC1), trainImg;
    for (int i = 0; i < classSum; i++)
    {
        //目标文件夹路径
        std::string inPath = "D:\\cplusplus_program\\opencvtest\\opencvtest\\charSamples\\";
        char temp[256] ;
        char lable[34] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',
            'G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z' };
        int k = 0;
        sprintf_s(temp, "%c", lable[i]);
        inPath = inPath + temp + "\\*.png";
        //用于查找的句柄
        long long handle;
        struct _finddata_t fileinfo;
        //第一次查找
        handle = _findfirst(inPath.c_str(), &fileinfo);
        if (handle == -1)
            return -1;
        do
        {
            //找到的文件的文件名
            std::string imgname = "D:\\cplusplus_program\\opencvtest\\opencvtest\\charSamples\\";
            imgname = imgname + temp + "\\" + fileinfo.name;

            Mat src = imread(imgname, 0);
            /*namedWindow("My Test", CV_WINDOW_AUTOSIZE);
             imshow("My Test", src);
             waitKey(0);*/
            if (src.empty())
            {
                std::cout << "can not load image \n" << std::endl;
                return -1;
            }
            //将所有图片大小统一转化为8*16
            resize(src, resizeImg, Size(imageRows, imageCols), (0, 0), (0, 0), INTER_AREA);
            threshold(resizeImg, trainImg, 0, 255, THRESH_BINARY | THRESH_OTSU);
            float* data = trainingData.ptr<float>(i * imagesSum + k);
            for (int j = 0; j < imageRows * imageCols; j++)
            {
                data[j] = (float)resizeImg.data[j];
                ;
                //trainingData[i * imagesSum + k][j] = (float)resizeImg.data[j];
            }
            // 设置标签数据
            uchar* data_lable = labels.ptr<uchar>(i * imagesSum + k);
            for (int j = 0;j < classSum; j++)
            {
                if (j == i)
                    data_lable[j] = 1;
                //labels[i * imagesSum + k][j] = 1;
                else
                    data_lable[j] = 0;
                //abels[i * imagesSum + k][j] = 0;
            }
            k++;

        } while (!_findnext(handle, &fileinfo));

        _findclose(handle);
    }
    //训练样本数据及对应标签
    Mat trainingDataMat(classSum * imagesSum, imageRows * imageCols, CV_32FC1, Scalar::all(0));
    trainingData.copyTo(trainingDataMat);
    //Mat trainingDataMat;
   // trainingData.copyTo(trainingDataMat);
    Mat labelsMat(classSum * imagesSum, (int)classSum, CV_32FC1, Scalar::all(0));
    //Mat labelsMat;
    labels.convertTo(labels, CV_32FC1);
    labels.copyTo(labelsMat);

    //std::cout << "trainingDataMat=" << trainingDataMat << "\n" << std::endl;
    std::cout << "labelsMat: \n" << labelsMat << "\n" << std::endl;
    //==========================训练部分==============================
    using namespace cv::ml;


    Mat layerSizes = (Mat_<int>(1, 4) << imageRows * imageCols, 128, 128, classSum);//输入层节点数为imageRows * imageCols，中间层为128，输出层为classSum
    //std::cout << "layerSizes: \n" << layerSizes << "\n" << std::endl;
    Ptr<ANN_MLP>model = ANN_MLP::create();//创建
    model->setLayerSizes(layerSizes);//设置层数
    model->setTrainMethod(ANN_MLP::BACKPROP, 0.001, 0.1);//训练方法,bp算法，setBackpropWeightScale for ANN_MLP::BACKPROP=0.001，setBackpropMomentumScale for ANN_MLP::BACKPROP=0.1
    model->setActivationFunction(ANN_MLP::SIGMOID_SYM, 1.0, 1.0);//激活函数
    model->setTermCriteria(TermCriteria(TermCriteria::MAX_ITER | TermCriteria::EPS, 10000, 0.0001));
    Ptr<TrainData> trainData = TrainData::create(trainingDataMat, ROW_SAMPLE, labelsMat);
    //Ptr<ml::TrainData>  trainData = ml::TrainData::create(trainingData, ROW_SAMPLE, labels);//创建训练数据

    //model->train(trainData);
    double time0 = static_cast<double>(getTickCount());
    model->train(trainData);
    //【5】计算运行时间并输出
    time0 = ((double)getTickCount() - time0) / getTickFrequency();
    cout << "\t训练完成，所用时间为： " << time0 << "秒" << endl;  //输出运行时间

    //保存训练结果
    model->save("MLPModel.xml");


    //==========================预测部分==============================
        //读取测试图像
    Mat test, dst;
    test = imread("D://cplusplus_program//opencvtest//opencvtest//N5.jpg", 0);;
    if (test.empty())
    {
        std::cout << "can not load image \n" << std::endl;
        return -1;
    }
    //将测试图像转化为1*128的向量
    resize(test, test, Size(imageRows, imageCols), (0, 0), (0, 0), INTER_AREA);
    threshold(test, test, 0, 255, THRESH_BINARY | THRESH_OTSU);
    Mat_<float> testMat(1, imageRows * imageCols);
    for (int i = 0; i < imageRows * imageCols; i++)
    {
        testMat.at<float>(0, i) = (float)test.at<uchar>(i / 8, i % 8);
    }
    //使用训练好的MLP model预测测试图像
    model->predict(testMat, dst);
    std::cout << "testMat: \n" << testMat << "\n" << std::endl;
    std::cout << "dst: \n" << dst << "\n" << std::endl;
    double maxVal = 0;
    Point maxLoc;
    minMaxLoc(dst, NULL, &maxVal, NULL, &maxLoc);
    std::cout << "测试结果：" << maxLoc.x << "置信度:" << maxVal * 100 << "%" << std::endl;
    namedWindow("原图", WINDOW_AUTOSIZE);
    imshow("原图", test);

}