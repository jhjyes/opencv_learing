#include "ep_1.h"

using namespace std;
using namespace cv;

void ep_1() {
	// read image
	Mat image = imread("test.jpg");//创建一个mat类的对象image，并读入图片
	if (image.empty())//如果没有读到图片，则为true
	{
		std::cout << "图片读取错误" << std::endl;
	}

	// 对图像进行所有像素用 （255- 像素值）
	Mat invertImage;
	image.copyTo(invertImage);//把image内容复制到invertimage
	
	// 获取图像宽、高
	int channels = image.channels();//获取通道数量
	int rows = image.rows;//获取行数
	std::cout << "rows=" << rows << std::endl;
	int cols = image.cols * channels;
	std::cout << "image.cols=" << image.cols << std::endl;
	std::cout << "cols=" << cols << std::endl;
	if (image.isContinuous()) {//判断存储是否连续
		cols *= rows;
		rows = 1;
		std::cout << "存储联连续" << std::endl;
		//连续的情况下，数据被保存在1行，行*列*通道数的值的列中
	}

	// 每个像素点的每个通道255取反
	uchar* p1;
	uchar* p2;
	for (int row = 0; row < rows; row++) {
		p1 = image.ptr<uchar>(row);// 获取行指针
		p2 = invertImage.ptr<uchar>(row);
		for (int col = 0; col < cols; col++) {
			*p2 = 255 - *p1; // 取反
			p2++;
			p1++;
		}
	}

	// create windows 
	namedWindow("My Test", WINDOW_AUTOSIZE);
	namedWindow("My Invert Image", WINDOW_AUTOSIZE);

	// display image
	imshow("My Test", image);
	imshow("My Invert Image", invertImage);

	// 关闭
	waitKey(0);
	destroyWindow("My Test");
	destroyWindow("My Invert Image");
}