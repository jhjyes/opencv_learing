#include "ep_3.h"
using namespace cv;
using namespace std;
void ep_3() {
	VideoCapture testvideo("1.avi");//loading video
	VideoCapture camre(0);

	while (1) {
		Mat frame;
		testvideo >> frame; //读取当前帧
		imshow("readvideo", frame);//显示当前帧
		
		Mat frame2;
		camre >> frame2;
		imshow("摄像头视频", frame2);

		Mat edge, grayimg;
		cvtColor(frame, grayimg, COLOR_BGR2GRAY);//转为灰度图像
		imshow("输出灰度图像", grayimg);
		blur(grayimg, edge, Size(3, 3));//使用3*3内核降噪
		Canny(edge, edge, 3, 9, 3);//运行canny算子
		imshow("边缘检测", edge);


		Mat edge2, grayimg2;
		cvtColor(frame2, grayimg2, COLOR_BGR2GRAY);//转为灰度图像
		imshow("camre输出灰度图像", grayimg2);
		blur(grayimg2, edge2, Size(3, 3));//使用3*3内核降噪
		Canny(edge2, edge2, 3, 9, 3);//运行canny算子
		imshow("camre边缘检测", edge2);
		waitKey(30);
	}
}