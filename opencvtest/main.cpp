#include<opencv2\opencv.hpp>


#include <tbb/tbb.h>
#include <iostream>
#include "ep_27.h"
using namespace cv;
/*

ep_1:图像显示
ep_2:图像腐蚀，模糊，边缘检测
ep_3:视频播放，摄像头采集
ep_4:彩色目标追踪
ep_5:光流optical flow
ep_6:点追踪ikdemo
ep_7:人脸识别
ep_8:svm分类
ep_9:无监督分类 
ep_10:生成图片并写入目录
ep_11:图像混合,并设置相对透明度
ep_12:鼠标基本操作
ep_13:格式化输出
ep_14:基本图像绘制
ep_15;访问像素的三种方法及时间消耗
ep_16:利用感兴趣区域进行线性图像混合
ep_17:利用机器学习+神经网络识别车牌数字
ep_18:手写数字识别
ep_19:线性滤波：方框滤波，均值滤波，高斯滤波
ep_20:离散傅里叶变换
ep_21:输入输出xml和yaml文件
ep_22:非线性滤波;中值滤波，双边滤波
ep_23:腐蚀（erode）与膨胀（dilate）
ep_24:开运算，闭运算，形态学梯度，顶帽，黑帽
ep_25:漫水填充
ep_26:resize图像调整示例
ep_27:pyrUp(),pryDown()图像金字塔
*/
int main(int argc, char** argv)
{



	
		
	ep_27();
	return 0;
 }
