#include "ep_8.h"

using namespace std;
using namespace cv;
using namespace cv::ml;
void ep_8() {
	// 视觉表达数据的设置（Data for visual representation）
	int width = 512, height = 512;
	Mat image = Mat::zeros(height, width, CV_8UC3);

	//建立训练数据（ Set up training data）
	int labels[7] = { 1, -1, -1, -1,1,1,1 };
	Mat labelsMat(7, 1, CV_32SC1, labels);
	
	float trainingData[7][2] = { { 501, 10 },{ 255, 10 },{ 501, 255 },{ 10, 501 },{255.100 },{355,30},{365,10} };
	Mat trainingDataMat(7, 2, CV_32FC1, trainingData);

	

	//设置支持向量机的参数（Set up SVM's parameters）
	/*SVM::Params params;*///原书不兼容
	/*Ptr<cv::ml::SVM> params = cv::ml::SVM::create();
	params->setType = SVM::C_SVC;
	params->setKernel = SVM::LINEAR;
	params->setTermCriteria = TermCriteria(TermCriteria::MAX_ITER, 100, 1e-6);*/
	
	Ptr<cv::ml::SVM> svm = cv::ml::SVM::create();
	svm->setType(cv::ml::SVM::Types::C_SVC);
	svm->setKernel(cv::ml::SVM::KernelTypes::LINEAR);
	svm->setTermCriteria(cv::TermCriteria(cv::TermCriteria::MAX_ITER, 100, 1e-6));
	// 训练支持向量机（Train the SVM）
	//Ptr<SVM> svm = params->train(trainingDataMat, ROW_SAMPLE, labelsMat, params);
	svm->train(trainingDataMat, cv::ml::SampleTypes::ROW_SAMPLE, labelsMat);
	Vec3b green(0, 255, 0), blue(255, 0, 0);
	//显示由SVM给出的决定区域 （Show the decision regions given by the SVM）
	for (int i = 0; i < image.rows; ++i)
		for (int j = 0; j < image.cols; ++j)
		{
			Mat sampleMat = (Mat_<float>(1, 2) << j, i);
			float response = svm->predict(sampleMat);

			if (response == 1)
				image.at<Vec3b>(i, j) = green;
			else if (response == -1)
				image.at<Vec3b>(i, j) = blue;
		}

	//显示训练数据 （Show the training data）
	int thickness = -1;
	int lineType = 8;
	circle(image, Point(255, 100), 5, Scalar(0, 0, 0), thickness, lineType);
	circle(image, Point(355, 30), 5, Scalar(0, 0, 0), thickness, lineType);
	circle(image, Point(501, 10), 5, Scalar(0, 0, 0), thickness, lineType);
	circle(image, Point(365, 10), 5, Scalar(0, 0, 0), thickness, lineType);
	circle(image, Point(255, 10), 5, Scalar(255, 255, 255), thickness, lineType);
	circle(image, Point(501, 255), 5, Scalar(255, 255, 255), thickness, lineType);
	circle(image, Point(10, 501), 5, Scalar(255, 255, 255), thickness, lineType);

	//显示支持向量 （Show support vectors）
	thickness = 2;
	lineType = 8;
	Mat sv = svm->getSupportVectors();

	for (int i = 0; i < sv.rows; ++i)
	{
		const float* v = sv.ptr<float>(i);
		circle(image, Point((int)v[0], (int)v[1]), 6, Scalar(128, 128, 128), thickness, lineType);
	}

	imwrite("result.png", image);        // 保存图像

	imshow("SVM Simple Example", image); // 显示图像
	waitKey(0);

}