#include "ep_24.h"
using namespace cv;
using namespace std;

static int SlectType = 0;
static int kernlSize = 3;

static Mat srcimg = imread("doge.jpg");
static Mat dstimg = srcimg.clone();

void ep_24()
{
	Mat srcimg = imread("doge.jpg");
	namedWindow("ԭʼͼ", WINDOW_AUTOSIZE);
	imshow("ԭʼͼ", srcimg);

	namedWindow("Ч��ͼ", WINDOW_AUTOSIZE);
	
	createTrackbar("��ʴ�����ͣ������㣬�����㣬��̬ѧ�ݶȣ���ñ����ñ", "Ч��ͼ", &SlectType, 6, on_ValueChange);
	on_ValueChange(SlectType, 0);
	createTrackbar("�ں�ֵ", "Ч��ͼ", &kernlSize, 21, on_ValueChange);
	on_ValueChange(kernlSize, 0);

	waitKey(0);
}
void on_ValueChange(int, void*) {
	/*�鿴��ض����֪��MORPH_OPEN��int�͵ı������ұ������Ĭ��ֵ��ʾ����Ĭ��ֵ���Ӧ���ʴ˴���ʡ��*/
	/*
	*  MORPH_ERODE    = 0, //!< see #erode
    MORPH_DILATE   = 1, //!< see #dilate
    MORPH_OPEN     = 2, //!< an opening operation
                        //!< \f[\texttt{dst} = \mathrm{open} ( \texttt{src} , \texttt{element} )= \mathrm{dilate} ( \mathrm{erode} ( \texttt{src} , \texttt{element} ))\f]
    MORPH_CLOSE    = 3, //!< a closing operation
                        //!< \f[\texttt{dst} = \mathrm{close} ( \texttt{src} , \texttt{element} )= \mathrm{erode} ( \mathrm{dilate} ( \texttt{src} , \texttt{element} ))\f]
    MORPH_GRADIENT = 4, //!< a morphological gradient
                        //!< \f[\texttt{dst} = \mathrm{morph\_grad} ( \texttt{src} , \texttt{element} )= \mathrm{dilate} ( \texttt{src} , \texttt{element} )- \mathrm{erode} ( \texttt{src} , \texttt{element} )\f]
    MORPH_TOPHAT   = 5, //!< "top hat"
                        //!< \f[\texttt{dst} = \mathrm{tophat} ( \texttt{src} , \texttt{element} )= \texttt{src} - \mathrm{open} ( \texttt{src} , \texttt{element} )\f]
    MORPH_BLACKHAT = 6, //!< "black hat"
                        //!< \f[\texttt{dst} = \mathrm{blackhat} ( \texttt{src} , \texttt{element} )= \mathrm{close} ( \texttt{src} , \texttt{element} )- \texttt{src}\f]
    MORPH_HITMISS  = 7  //!< "hit or miss"
                        //!<   .- Only supported for CV_8UC1 binary images. A tutorial can be found in the documentation
	*/
	/*switch (SlectType)
	{
	case 0:
		int _SlectType = MORPH_OPEN;
		break;
	case 1:
		int _SlectType = MORPH_CLOSE;
		break;
	
	}*/
	
	Mat element = getStructuringElement(MORPH_RECT, Size(2 * kernlSize + 1, 2 * kernlSize + 1));
	morphologyEx(srcimg, dstimg, SlectType, element);
	imshow("Ч��ͼ", dstimg);
}