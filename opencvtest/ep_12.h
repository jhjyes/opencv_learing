#ifndef _ep_12_h
#define _ep_12_h
#include<opencv.hpp>
#include<core/types.hpp>
using namespace cv;
using namespace std;
#define WINDOW_NAME "【程序窗口】" 
//-----------------------------------【全局函数声明部分】------------------------------------
//		描述：全局函数的声明
//------------------------------------------------------------------------------------------------
void on_MouseHandle(int event, int x, int y, int flags, void* param);
void DrawRectangle(cv::Mat& img, cv::Rect box);

void ep_12();


#endif // !_ep_12_h

#pragma once
