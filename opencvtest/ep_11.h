#ifndef _ep_11_h_
#define _ep_11_h_
#include <opencv.hpp>

extern cv::Mat img;
extern cv::Mat logo;
extern cv::Mat add_logo;
extern const int g_Max_Value;//滑动条的最大值
extern int g_trackbar_varity;//滑动条对应的变量

void ep_11();
void on_trackbar(int, void*);
#endif // !_ep_11_h_

